import argparse
# from pythonosc import osc_message_builder
# from pythonosc import udp_client

#Use Command Line Arguments to point to the server
#If none are provided fallback to defaults (in this case our server, lucky us! :D)
parser = argparse.ArgumentParser()

parser.add_argument("--ip", default="192.168.1.29")
parser.add_argument("--port", type=int, default=12000)

#Read parameters for OSC communication from the command line
#in the order:
# -Light Number
# -R
# -G
# -B
#
#ie. python ./changeLights.py 1 255 40 30
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                    help='RGB values')

# args = parser.parse_args()
# client = udp_client.SimpleUDPClient(args.ip, args.port)

# #this function changes [light_id]'s RGB value to its given parameters
# def light_change (light_id, r, g, b, intenz = 1.0):
#     addr = "/spermateo/l"+str(light_id)
#     values = [intenz, r, g, b]
#     client.send_message(addr, values)
#     print("cambiata light_id "+str(light_id))


# #call the light_change function with data from the Command Line
# #TODO: integrity check on CLI parameters
# light_change (args.integers[0], args.integers[1], args.integers[2], args.integers[3])

#goodbye!
