import netP5.*;
import oscP5.*;

OscP5 oscP5;
float background_value = 0;
float circle_size_value = 0;

void setup() {
  size(600,600);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,12000);
  background(0);
}

void draw() {
  background(background_value);
  ellipseMode(RADIUS);
  strokeWeight(1);
  stroke(255-background_value);
  noFill();
  for(int i = 0; i < 100; i++){
    ellipse(width/4+map(circle_size_value,0,300,width/4,0),height/2,circle_size_value/5*i,circle_size_value/5 * i);
    ellipse(width*3/4-map(circle_size_value,0,300,width/4,0),height/2,circle_size_value/5*i,circle_size_value/5 * i);
  }
}

void oscEvent(OscMessage message) {
  if(message.addrPattern().equals("/background")){ // OSC channel '/background' range [0,255]
    background_value = background_value * 0.5 + (float)message.arguments()[0] * 0.5;
  }
  if(message.addrPattern().equals("/circle_size")){ // OSC channel '/circle_size' range [0,300]
    circle_size_value = circle_size_value * 0.5 + (float)message.arguments()[0] * 0.5;
  }
}