/**
 * Example created by Bruno
 */
 
import oscP5.*;
import netP5.*;

OscP5 oscP5;

PVector last_location = new PVector(0,0,0);
int last_polygon = 0;
float polygon_radius = 20;

void setup() {
  size(400,400);
  frameRate(25);
  
  final String OS = platformNames[platform];
  if(OS == "macosx"){ // force IPv4 for Mac OSX
    System.setProperty("java.net.preferIPv4Stack", "true");
  }
  
  /* create a new instance of oscP5 using a multicast socket. */
  oscP5 = new OscP5(this,"239.0.0.1",12000);

  background(0);
  noStroke();
  frameRate(15);
}


void draw() {
  fill(0, 6);
  rect(0,0,width, height);
  if (last_polygon != 0){
    fill(random(255), random(255), random(255));
    draw_polygon(last_location.x, last_location.y, polygon_radius+last_location.z, last_polygon);
    last_polygon = 0;
  }
}




/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage inMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: "+inMessage.addrPattern());
  println(" typetag: "+inMessage.typetag());
  
  if (inMessage.addrPattern().equals("/polygon")){
    if (inMessage.typetag().equals("iiii")){
      //println("Matching msg");
      last_location = new PVector(inMessage.get(0).intValue(),
                                  inMessage.get(1).intValue(),
                                  inMessage.get(2).intValue());
                                  
      last_polygon = inMessage.get(3).intValue();
      
    } else {
      println("received polygon message with incorrect typetag"); 
    }
  }
}

void mousePressed() {
  /* Create a message and fill it with the polygon parameters */
  OscMessage outMessage = new OscMessage("/polygon");
  
  outMessage.add(mouseX); 
  outMessage.add(mouseY); 
  outMessage.add((int)random(20));
  outMessage.add((int)random(9)); 
  /* send the message */
  oscP5.send(outMessage); 
}