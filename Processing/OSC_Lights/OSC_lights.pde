/**
 * Example created by Bruno laurencich
 * based on oscP5sendreceive by andreas schlegel
 */
 
import oscP5.*;
import netP5.*;
  
color colors[] = new color[4];

OscP5 oscP5;
NetAddress myRemoteLocation;

String base_address = "lights";
//This is a regex pattern: a fancy and very convenient way of doing string matching
String matching_pattern = "^/" + base_address+ "/[lLnN]([1-4])";


void setup() {
  size(400,140);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,12000);
  
  for (int i =0; i<4 ; i++){
    colors[i] = color(i * 80 ,0, 50);
  }
}


void draw() {
  background(0);  
  
  for (int i =0; i<4 ; i++){
    fill(colors[i]);
    ellipse(  i * width/4 + width/8, 70, 60, 60);
  }
}


/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  //Check if the addres match our expected pattern
  //if it does we get returned an array containing the entire match, and all the capture groups (inside parentesis)
  String[] m = match(theOscMessage.addrPattern(), matching_pattern);
  if (m != null){
    //use the first capture group as the index of our color array
    int nLight = int(m[1]) -1;
    colors[nLight] = color( theOscMessage.get(0).intValue(),
                            theOscMessage.get(1).intValue(),
                            theOscMessage.get(2).intValue());

    println("Setting color on light " + m[1]);
    
  } else {
    /* print the address pattern and the typetag of the received OscMessage */
    print("### received a non matching osc message.");
    print(" addrpattern: "+theOscMessage.addrPattern());
    println(" typetag: "+theOscMessage.typetag());

  }
  
}