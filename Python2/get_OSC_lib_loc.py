import OSC
from sys import exit
from os.path import isfile

loc = OSC.__file__

if not loc:
	exit(1)

if loc.endswith(".pyc"):
	script = loc.rstrip(".pyc") + ".py"
	if isfile(script):
		print script
		exit(0)
	else:
		exit(1)
else:
	exit(1)

