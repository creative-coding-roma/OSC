#!/usr/bin/python
from OSC import *
import argparse


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Send a test OSC message')

	parser.add_argument('-i','--ip', help='the destination ip', default="127.0.0.1")
	parser.add_argument('-p','--port', help='the destination port', default=6565, type=int)
	parser.add_argument('-a','--address', help='the OSC address', default="/test")
	parser.add_argument('--int', action='store_true',
                    help='send as integers')
	parser.add_argument('--float',  action='store_true',
                    help='send as floats')
	parser.add_argument('msg', nargs='+',
                    help='arguments to send')
	args = parser.parse_args()

	c = OSCClient()
	c.connect((args.ip, args.port))

	msg = OSCMessage()
	msg.setAddress(args.address)

	typetag = ""
	for i,m in enumerate(args.msg):
		if args.int:
			args.msg[i] = m = int(m)
			typetag += "i"
		elif args.float:
			args.msg[i] = m = float(m)
			typetag += "f"
		else:
			typetag += "s"


		msg.append(m)

	print "Sending: {} | {} <{}> {}".format((args.ip, args.port), args.address, typetag, args.msg )

	c.send(msg)
