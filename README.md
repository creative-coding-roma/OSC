# OSC examples

Some examples to demonstrate remote control through OSC protocol, written in:

- Processing
- Supercollider
- PureData
- Python

-----	

## Processing

#### OSC_Moire

**Control the distance of two centers of a radial Moire pattern**
Usage: Find out the local ip address of the pc running the sketch. The listening port is 12000. 

Use another application to send the messages:

- `/background <f> (color)`
- `/circle_size <f> (size)`

#### OSC_Lights

**Control the color of 4 lights**
Usage: Find out the local ip address of the pc running the sketch. The listening port is 12000. 

Use another application to send the messages:

- `/light/n# <iii> (red_val),(green_val),(blue_val)`

(Replace the _#_ with a number btw 1 and 4 on the address pattern.)	

#### OSC_poligons

**This sketch demonstrates the UDP multicasting capabilities**.

Usage: Open the sketch and click at some point of the window to draw a poligon on all the sketchs listening on the same LAN.

-----	

## Supercollider

OSC_Out: example of sending (to PureData) Osc messages

OSC_In: example of creating a synth where the frequency can be changed by controlling OSC messages (from PureData)

-----	

## PureData

PDinOSC: Example of receiving OSC messages and numeric display of the value contained in the message

PDoutOSC: Example of sending (towards Supercollider) of OSC messages through numerical values or slider

-----	
## Python3_pyosc

_using [pythonosc](https://github.com/attwad/python-osc) library (straight forward lib, suitable for most cases)_


#### changelights.py 

**Send messages to control the [OSC_Lights](#osc_lights) processing sketch**

Usage: 
Read parameters for OSC communication from the command line
in the order:
 - Light Number
 - R
 - G
 - B

```bash
python ./changeLights.py <Light_Number> <R value> <R value> <G value> <B value>
```

-----	
## Python3_osc4py3

_using [osc4py3](http://osc4py3.readthedocs.io/en/latest/index.html) library (more complex lib, allowing much flexible networking, parsing and threading handling. It also suppors multicasting)_

#### test_receive.py and test_send.py 

**Send and receive to a multicast group**


-----	

## Python2

_using [pyOSC](https://pypi.org/project/pyOSC/) library_

#### osc_send_cli.py
**CLI utility for sending arbitrary OSC test messages**
(only string, int or floats supported)

Usage:

```
OscSend [-h] [-i IP] [-p PORT] [-a ADDRESS] [--int] [--float]
               msg [msg ...]

Send a test OSC message

positional arguments:
  msg                   arguments to send

optional arguments:
  -h, --help            show this help message and exit
  -i IP, --ip IP        the destination ip
  -p PORT, --port PORT  the destination port
  -a ADDRESS, --address ADDRESS
                        the OSC address
  --int                 send as integers
  --float               send as floats
```

If you use `pyenv` or something, or just don't want to install the library globally you can create a bundle running:

`make`

or

`make install` in order to create a symlink in your `.local/bin` folder (useful on some linux desktops)

after that you can just run `OscSend <args>`

#### osc_receive.py
**Receives messages under the `/test` addrpattern, and some default handlers provided by pyOSC**

#### osc_send_test.py
**Sends test messages under the `/test` addrpattern**