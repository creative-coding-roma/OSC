# Import needed modules from osc4py3
from osc4py3.as_eventloop import *
from osc4py3 import oscmethod as osm
import time

def handlerfunction(msg):
	print(msg)

def handlerfunction2(address, s, x, y):
	# Will receive message address, and message data flattened in s, x, y
	print(address, s, x, y)

# Start the system.
osc_startup()

# Make server channels to receive packets.
osc_udp_server("239.0.0.1", 12000, "aservername")
# osc_udp_server("0.0.0.0", 3724, "anotherserver")

# Associate Python functions with message address patterns, using default
# argument scheme OSCARG_DATAUNPACK.
osc_method("*", handlerfunction, argscheme=osm.OSCARG_MESSAGE)
# osc_method("/test/*", handlerfunction)
# Too, but request the message address pattern before in argscheme
osc_method("/test/*", handlerfunction2, argscheme=osm.OSCARG_ADDRESS + osm.OSCARG_DATAUNPACK)

# Periodically call osc4py3 processing method in your event loop.
finished = False

try:
	while not finished:
		# …
		osc_process()
		# …
		time.sleep(0.2)

except KeyboardInterrupt as e:
	print("terminating..")


# Properly close the system.
osc_terminate()
